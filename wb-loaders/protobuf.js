var loaderUtils = require("loader-utils");
var validateOptions = require('schema-utils');
var protoCli = require("protobufjs/cli");

module.exports = function(content) {
	  var callback = this.async();

	  protoCli.pbjs.main([ "--target", "json-module", "path/to/myproto.proto" ], function(err, output) {
        if (err) {
            callback(err);
        }
        else {
            callback(null, output);
        }
    });
};
