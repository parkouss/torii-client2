const HtmlWebPackPlugin = require("html-webpack-plugin");
const path = require('path');

module.exports = {
    resolveLoader: {
        modules: [
            'node_modules',
            path.resolve(__dirname, 'wb-loaders')
        ]
    },
    resolve: {
        extensions: ['.js', '.jsx', '.json', '.ts', '.tsx']
    },
    module: {
        rules: [
            {
                test: /\.(ts|tsx)$/,
                loader: 'ts-loader'
            },
            { enforce: "pre", test: /\.js$/, loader: "source-map-loader" },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: "html-loader"
                    }
                ]
            },
            {
                test: /\.proto$/,
                use: {
                    loader: 'protobuf',
                    options: {
                    }
                }
            }
        ]
    },
    plugins: [
        new HtmlWebPackPlugin({
            template: "./src/index.html",
            filename: "./index.html"
        })
    ]
};
