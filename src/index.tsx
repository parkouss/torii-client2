import FormContainer from "./js/components/container/FormContainer";
import * as ReactDOM from "react-dom";
import * as React from "react";
const Root = require("../proto/torii.proto");

ReactDOM.render(<FormContainer />, document.getElementById("create-article-form"));

export {Root};