import * as React from "react";
import Input from "../presentational/Input";

class FormContainer extends React.Component<{}, {seo_title: string}> {
    constructor(props: {}) {
        super(props);
        this.state = {
            seo_title: ""
        };
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(event: any) {
        this.setState({ seo_title: event.target.value });
    }
    render() {
        return (
            <form id="article-form">
              <Input
                text="SEO title"
                label="seo_title"
                type="text"
                id="seo_title"
                value={this.state.seo_title}
                handleChange={this.handleChange}
                />
            </form>
        );
    }
}

export default FormContainer;
