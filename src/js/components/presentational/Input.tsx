import * as React from "react";

interface InputProps {
    label: string;
    text: string;
    type: string;
    id: string;
    value: string;
    handleChange: (event: any) => any;
}

const Input = (props: InputProps) => (
    <div className="form-group">
      <label htmlFor={props.label}>{props.text}</label>
      <input
          type={props.type}
          className="form-control"
          id={props.id}
          value={props.value}
          onChange={props.handleChange}
          required
      />
    </div>
);
export default Input;
